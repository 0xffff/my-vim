
#plugin
##auto "" and () completion** 

##nerdtree
	*:B access bookmark
	*:Bookmark {name} add highlighted dir to bookmark
	*:u go up
	*:C set current dir to default dir

##tagbar
	*<c-p>: show source code class
	
##nerdcommenter
	*<leader>cs bulk comment selected lines
	*<leader>cc comment single line
	*<leader>ci toggle comment on individual line
	*<leader>c<space> toggle comment on selected lines

##tabular
	* VG to highlight block of code, :Tabularize/[symbol] 

##snipMate
	* fast tempalte generatation, dependings on two modules : tlib, vim-addon-mw-utils
	* >>> make sure set paste is commented out <<<


##window swap
	* basic mode: <leader>ww select , navigate to wanted position <leader>ww swap
	* advanced mode: <leader>yw yank window <leader>pw paste window

    
#colorscheme: default to tomorrow-night-eighties
	- gruvbox    

#a few tweaks
    - set cursorline
    - <Leader> key is mapped to ,
    - better window navigation: c-j, c-k, c-h, c-l
    - increased undo level (1000)
    - nerdtree auto start
    - auto enble mouse mode, to toggle mouse mode, use <c-M>
    - to toggle nerdTree use <c-N>
    - to toggle tagBar, use <c-P>, make sure install ctags first: yaourt -S ctags

#to download submodule
	- git submodule init
	- git submodule update

#to update submodule
	- git submodule foreach git pull origin master

#stuff in bundle
	- nerdTree
	- vim-airline
	- minibufexpl
	- vim-tagbar
	- nerdcommenter  
	- delimitMate
	- vim-airline-theme :AirlineTheme [themename]
	- vim-snipmate
		+ tlib
		+ vim-addon-wm-utils

