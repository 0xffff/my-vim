" execute pathogen#infect()
execute pathogen#infect('bundle/{}', '~/src/vim/bundle/{}')

syntax on
filetype plugin indent on
"keep cursor in the same column
set nosol
set backspace=indent,eol,start

"cursor line
hi CursorLine cterm=NONE ctermbg=darkred ctermfg=white guibg=darkred guifg=white


"automatic resize split
au VimResized * :wincmd =

"remember code folding 
au BufWinLeave ?* mkview
au BufWinLeave ?* silent loadview

nnoremap <c-d> <pageup> 
nnoremap <c-c> <pagedown>


"insert newline in normal mode

"set indent mode to indent 
set fdm=indent
set fdc=4
set fdl=1
"remap the program folding toggle"

"display line number
set number
set colorcolumn=100
noremap zz za
set backup
set writebackup
set noswapfile

"This unsets the "last search pattern" register by hitting return
"nnoremap <CR> :noh<CR><CR>

"no backup or swap file

"execute current file
nnoremap <silent> <leader>ee :!%:p<CR>


"miniBufExpl
nmap { :MBEbp<CR>
nmap } :MBEbn<CR>

"minibufexp
let g:miniBufExplMapWindowNavVim = 1 
let g:miniBufExplMapWindowNavArrows = 1 
let g:miniBufExplMapCTabSwitchBufs = 1 
let g:miniBufExplModSelTarget = 1 


"return to prevous file
nnoremap <c-q> :e#<CR>

let g:tabular_loaded = 1



"ignore case
"to make it case sensitive, :set noic
set ic
set smartindent
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab

" set paste

"toggle tagbar
nnoremap <silent> <c-P> :TagbarToggle<CR>
"autocmd vimenter * TagbarToggle 

"set t_Co=256

autocmd vimenter * NERDTree
"default cursor to text window
autocmd VimEnter * wincmd p
let NERDTreeDirArrow=0
"enable mouse in vim 
se mouse=a



"increase undo level
set undolevels=1000

"highlight serached pattern
set hlsearch
"highlight current cursor line
set cursorline

"##############################################################################                                                                         
"" Easier split navigation                                                                                                                               
"##############################################################################                                                                         
"" Use ctrl-[hjkl] to select the active split!
nmap <silent> <c-k> :wincmd k<CR>
nmap <silent> <c-j> :wincmd j<CR>
nmap <silent> <c-h> :wincmd h<CR>
nmap <silent> <c-l> :wincmd l<CR>


"adjust split size
nmap <silent> <c-o> <C-w><
nmap <silent> <c-i> <C-W>-
nmap <silent> <c-u> <C-W>+
nmap <silent> <c-y> <C-w>>

"remap leader key to ,
let mapleader = ","


"toggle NerdTree
nmap <silent> <c-n> :NERDTreeToggle<CR>
nmap <silent> <tab> <tab>

"close vim when there's no window left
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

set history=1000


"set title to hostname + file name
autocmd BufEnter * let &titlestring = "[vim(" . expand("%:t") . ")]"
set title

autocmd WinEnter * call s:CloseIfOnlyNerdTreeLeft()

" buffer that's left is the NERDTree buffer
function! s:CloseIfOnlyNerdTreeLeft()
	if exists("t:NERDTreeBufName")
		if bufwinnr(t:NERDTreeBufName) != -1
			if winnr("$") == 1 
                q 
            endif
		endif
	endif
endfunction

nnoremap <silent> <Leader>sh :shell <CR>


set background=dark
let g:gruvbox_contrast_dark='soft'
colorscheme gruvbox

"colorscheme gruvbox-material
" colorscheme desert
 "colorscheme dracula
" colorscheme gruvbox-material
" colorscheme gruv-hard
" colorscheme hybrid-material
" colorscheme hybrid-reverse
" colorscheme material
" colorscheme nefertiti
" colorscheme toychest
"colorscheme wildcherry
" colorscheme zenburn

let g:asyncrun_mode = 0
let g:asyncrun_open = 6

hi Normal guibg=NONE ctermbg=NONE

function! OpenInGrok()

	let f = expand("%:p")

	let prefix1="/x/eng/bbsvl/users/yilong"
	let prefix2="/x/eng/bbrtp/users/yilong"
	let $cmd="test"
	let start=len(prefix1)

	let i = stridx(f, prefix1)
	let j = stridx(f, prefix2)
	
	let part = ''

	if i >-1 || j > -1
		let z = stridx(f[start+1:], "/")
		let p=f[start+z+1:]		
		let url="http://opengrok-prd.eng.netapp.com/source/xref/dev".p
		let url= "firefox ".url."\#".line('.')
		let $cmd = url
			
		AsyncRun $cmd
			
	else
		echo "OpenInGrok: not in p4 work space"
	endif

	echo "end of function"
endfunction

nnoremap <leader>t :call OpenInGrok()<CR>

"copy to system clipboard
noremap <Leader>y "+y
noremap <Leader>p "+p

"show current function name
map <c-f> <Nop>
fun! ShowFuncName()
        let lnum = line(".")
        let col = col(".")
        echohl ModeMsg
        echo getline(search("^[^ \t#/]\\{2}.*[^:]\s*$", 'bW'))
        echohl None
        call search("\\%" . lnum . "l" . "\\%" . col . "c")
endfun
map f :call ShowFuncName() <CR>


"quick reload vimrc
nnoremap <Leader><C-f> :so $MYVIMRC<CR>
"quick edit vimrc
nnoremap <Leader><C-g> :split $MYVIMRC<CR>
